
### Goals
- The trainee will understand the basics of working with Git

### Tasks
- Read about Git, Explain the following:
  - What is a **Commit**?
  - What is a **Branch**?
  - What is the **HEAD**?
- Explain the concept of remote and local in Git 
- How does one create a new branch?
- How can you move between branches?
- What happens to the edited files when moving between different branches?
- Explain how does `git reset` work
- In your own repository, create a situation where there is a conflict between two branches, then resolve the conflicts and merge the branches
- Complete the [tutorial](https://learngitbranching.js.org/)
- At last create username in the closed network gitlab (ask your hofef for help). create a project, create a new branch and do at least 2 commits using gitlab web IDE, revert the last commit, create a merge request to main. delete the project.
- Go to a random git project, how can you know who changed a specific line in a file?
