### Goals
- The trainee will understand what are partitions, partitions tables and partition type
- The trainee will understand how to create and manage partitions 

### Tasks
- Read about:
  - Briefly anout GPT & MBR
  - Partition types (Primary and Extended)
  - `fdisk`
  - `parted`
  - `partprobe`
- What partitions are used for and why are they needed
- Create a partition on a disk with `parted` and on another disk with `fdisk`
- Reduce and increase the size of the partition once with `parted` and once with `fdisk`
- List all the partitions on a single disk 
