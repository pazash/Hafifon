### Goals
- The trainee will know the most popular Linux filesystems
- The trainee will understand the differences between different filesystems in Linux
- The trainee will learn how to fix a broken filesystems

### Tasks
- Read briefly about:   
  - inodes
  - superblocks
  - metadata
  - journal
- Explain the difference between hard link and soft link
- Create hard link and a soft link to the same file called 'Elinux'
- How can we find the inode of a specific file?
- Add a disk to your VM, create 2 partitions on it and create the folowing filesystems:
  - XFS
  - EXT4 with journal
- Extend the file systems you've created by 100MB
- Explain what happens when a file is created and deleted in a filesytem 
